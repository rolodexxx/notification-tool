import React, { useEffect, useState, useContext } from "react";
import Switch from "@material-ui/core/Switch";
import API from "../../APIs/index";
import { Button, Slider, Typography } from "@material-ui/core";
import { ModalContext } from "../../Contexts/ModalContext";
import { SnackBarContext } from "../../Contexts/SnackBarContext";

export default function Options(props: any) {
  let userId = props.userId;
  const {
    handleClose,
    userPreference,
    setUserPreference,
    reminderPreference,
    setReminderPreference,
  } = useContext(ModalContext);

  const { setMessage, setToast } = useContext(SnackBarContext);
  const [userSettings, setUserSettings] = useState({
    first_name: "",
    last_name: "",
    profile_id: null,
  });

  useEffect(() => {
    let getDetails = async () => {
      let res = await API.get(`/cs/user/${userId}`);
      await setUserSettings(res.data[0]);
      res = await API.get(`/cs/user/preference/${userId}`);
      try {
        setReminderPreference(res.data[0].reminder_time);
        setUserPreference(res.data[0].preference_obj);
      } catch {}
    };
    getDetails();
  }, [userId, setReminderPreference, setUserPreference]);

  const confirmChanges = async () => {
    try {
      await API.put(`/cs/user/${userId}`, {
        userPreference: JSON.stringify(userPreference),
        reminderPreference: JSON.stringify(reminderPreference),
        notificationType: "Reminder",
      });
      // console.log("this is what I'm sending\n", {
      //   userPreference: JSON.stringify(userPreference),
      //   reminderPreference: JSON.stringify(reminderPreference),
      //   notificationType: "Reminder",
      // });
      handleClose();
      setMessage("Preferences updated successfully");
      setToast(true);
    } catch (error) {
      setMessage("Something went wrong..." + error);
      setToast(true);
    }
  };

  return (
    <div className="w-100">
      <h2 id="modal-title">
        Edit Preferences for{" "}
        <span className="text-primary">
          {userSettings.first_name} {userSettings.last_name}
        </span>
      </h2>
      <hr />
      <b className="mt-3 mb-0">Notification preferences</b>

      <form className="mt-3">
        {userSettings.profile_id === 1 ||
          (userSettings.profile_id === 5 && (
            <p>Support for this user type will be added in a later feature</p>
          ))}

        <ul>
          {userSettings.profile_id === 4 && (
            <>
              <li className="d-flex align-items-baseline justify-content-between">
                <label>Job start alerts</label>
                <Switch
                  onChange={() =>
                    setUserPreference({
                      ...userPreference,
                      jobStart: !userPreference.jobStart,
                    })
                  }
                  color="primary"
                  checked={userPreference.jobStart}
                ></Switch>
              </li>
              <li className="d-flex align-items-center justify-content-between">
                <Typography id="discrete-slider" gutterBottom>
                  Reminder period
                </Typography>
                <Slider
                  defaultValue={reminderPreference.jobStartTime}
                  key={`slider-${reminderPreference.jobStartTime}`}
                  step={15}
                  marks
                  valueLabelDisplay="auto"
                  min={15}
                  max={60}
                  onChange={(event, newValue) => {
                    setReminderPreference({
                      ...reminderPreference,
                      jobStartTime: newValue,
                    });
                  }}
                />
              </li>
              <li className="d-flex align-items-baseline justify-content-between mt-3">
                <label>Job Due alerts</label>
                <Switch
                  onChange={() =>
                    setUserPreference({
                      ...userPreference,
                      jobDue: !userPreference.jobDue,
                    })
                  }
                  checked={userPreference.jobDue}
                  color="primary"
                ></Switch>
              </li>
              <li className="d-flex align-items-center justify-content-between">
                <Typography id="discrete-slider" gutterBottom>
                  Reminder period
                </Typography>
                <Slider
                  defaultValue={reminderPreference.jobDueTime}
                  key={`slider-${reminderPreference.jobDueTime}`}
                  step={15}
                  marks
                  valueLabelDisplay="auto"
                  min={15}
                  max={60}
                  onChange={(event, newValue) => {
                    setReminderPreference({
                      ...reminderPreference,
                      jobDueTime: newValue,
                    });
                  }}
                />
              </li>
            </>
          )}
          {userSettings.profile_id === 3 && (
            <>
              <li className="d-flex align-items-baseline justify-content-between mt-3">
                <label>Job Missed alerts</label>
                <Switch
                  onChange={() =>
                    setUserPreference({
                      ...userPreference,
                      jobMissed: !userPreference.jobMissed,
                    })
                  }
                  checked={userPreference.jobMissed}
                  color="primary"
                ></Switch>
              </li>
            </>
          )}

          {userSettings.profile_id === 2 && (
            <>
              <li className="d-flex align-items-baseline justify-content-between mt-3">
                <label>Job Status alerts</label>
                <Switch
                  onChange={() =>
                    setUserPreference({
                      ...userPreference,
                      jobStatus: !userPreference.jobStatus,
                    })
                  }
                  checked={userPreference.jobStatus}
                  color="primary"
                ></Switch>
              </li>
              <li className="d-flex align-items-center justify-content-between">
                <Typography id="discrete-slider" gutterBottom>
                  Reminder period
                </Typography>
                <Slider
                  defaultValue={reminderPreference.jobStatusTime}
                  key={`slider-${reminderPreference.jobStatusTime}`}
                  step={15}
                  marks
                  valueLabelDisplay="auto"
                  min={15}
                  max={60}
                  onChange={(event, newValue) => {
                    setReminderPreference({
                      ...reminderPreference,
                      jobStatusTime: newValue,
                    });
                  }}
                />
              </li>
            </>
          )}
        </ul>
      </form>
      <Button
        type="button"
        variant="outlined"
        color="secondary"
        onClick={handleClose}
      >
        Close
      </Button>
      <Button
        className="mx-2"
        variant="outlined"
        type="button"
        color="primary"
        onClick={confirmChanges}
        disabled={
          userSettings.profile_id === 1 || userSettings.profile_id === 5
        }
      >
        Save changes
      </Button>
    </div>
  );
}

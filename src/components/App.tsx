import React from "react";
import Dashboard from "./Dashboard/Dashboard";
import { Container } from "@material-ui/core";
import SnackBar from "./Utilities/Snackbar";
import { SnackBarContext } from "../Contexts/SnackBarContext";

function App() {
  let [toast, setToast] = React.useState(false);
  let [message, setMessage] = React.useState("");

  return (
    <Container>
      <SnackBarContext.Provider value={{ setToast, setMessage }}>
        <SnackBar show={toast} message={message} />
        <h1 className="display-4 mt-3">Preference Manager</h1>
        <Dashboard></Dashboard>
      </SnackBarContext.Provider>
    </Container>
  );
}

export default App;

import React, { useState } from "react";
import Table from "./Table";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { ModalContext } from "../../Contexts/ModalContext";
import Options from "../Preferences/Options";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: "1px solid #a5a5a5",
      padding: "2rem 3rem",
      borderRadius: "5px",
    },
  })
);

const Dashboard = () => {
  const classes = useStyles();
  const [selected, setSelected] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [userPreference, setUserPreference] = useState<any | any[]>({
    jobStart: true, // technician
    jobDue: true, // technician
    jobMissed: true, // service manager
    jobStatus: true, // dispatcher
    SAExpiry: true, // dispatcher
  });

  const [reminderPreference, setReminderPreference] = useState<any | any[]>({
    jobStartTime: 30, // technician
    jobDueTime: 15, // technician
    jobStatusTime: 30, // dispatcher
  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <ModalContext.Provider
        value={{
          setSelected,
          handleOpen,
          handleClose,
          reminderPreference,
          setReminderPreference,
          userPreference,
          setUserPreference,
        }}
      >
        <Table />
        <Modal
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div className={classes.paper}>
              <Options userId={parseInt(selected[0])} />
            </div>
          </Fade>
        </Modal>
      </ModalContext.Provider>
    </>
  );
};

export default Dashboard;

import React, { useContext, useState, useEffect } from "react";
import { DataGrid, ColDef } from "@material-ui/data-grid";
import { ModalContext } from "../../Contexts/ModalContext";
import API from "../../APIs";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    div: {
      height: "50rem",
      Width: "100%",
    },
  })
);

const columns: ColDef[] = [
  { field: "id", headerName: "ID" },
  {
    field: `fullName`,
    headerName: "Name",
    width: 200,
    valueGetter: (params) =>
      `${params.getValue("first_name") || ""} ${
        params.getValue("last_name") || ""
      }`,
  },
  {
    field: "username",
    headerName: "Email Address",
    width: 200,
  },
  {
    field: "profile_id",
    headerName: "Role",
    width: 200,
  },
  {
    field: "cellphone",
    headerName: "Phone number",
    width: 200,
  },
  {
    field: "landline",
    headerName: "Landline",
    width: 200,
  },
];

export default function Table() {
  const classes = useStyles();
  const { setSelected, handleOpen } = useContext(ModalContext);
  const [rows, setRows] = useState([]);

  useEffect(() => {
    const fetchInfo = async () => {
      let res = await API.get("/cs/user");
      setRows(res.data);
    };
    fetchInfo();
  }, []);

  return (
    <div className={classes.div}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={15}
        // checkboxSelection
        disableMultipleSelection={true}
        onSelectionChange={(selectionChangeParams) => {
          setSelected(selectionChangeParams.rowIds);
          handleOpen();
        }}
        hideFooterSelectedRowCount
      />
    </div>
  );
}
